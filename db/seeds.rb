# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "password",
             password_confirmation: "password",
             admin:        true,
             activated:    true,
             activated_at: Time.zone.now)

user_active = true

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "Password1"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated:    user_active,
               activated_at: user_active ? Time.zone.now : nil)
  user_active = !user_active
  puts user_active
end

users = User.where(activated: true).order(:created_at).take(6)
50.times do
  users.each { |user| user.microposts.create!(content: Faker::Lorem.sentence(5)) }
end

User.create!(name:  "Admin User",
             email: "admin@mondegreen.com",
             password:              "password",
             password_confirmation: "password",
             admin:        true,
             activated:    true,
             activated_at: Time.zone.now)

User.create!(name:  "Other User",
             email: "other@mondegreen.com",
             password:              "password",
             password_confirmation: "password",
             admin:        false,
             activated:    true,
             activated_at: Time.zone.now)

# set up some following relationships
users = User.where(activated: true)
user  = users.first
following = users[2..30]
followers = users[3..20]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }