# Mondegreen: sample application

This is the sample base application for Mondegreen development.  This has
been based on the starting tutorial application by Micheal Hartl: 
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/).