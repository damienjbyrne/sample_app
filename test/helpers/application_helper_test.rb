require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "Full title helper" do
    assert_equal full_title,         'Mondegreen'
    assert_equal full_title("Help"), 'Help | Mondegreen'
  end
end
