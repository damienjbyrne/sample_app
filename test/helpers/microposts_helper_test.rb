require 'test_helper'

class MicropostsHelperTest < ActionView::TestCase
  test "Render URLs as links with parse_content" do
    url_text = "http://example.com"
    text_data = "This is a URL #{url_text} to be linked"
    modified_text = parse_content text_data
    assert_not_equal text_data, modified_text
    #doc = HTML::Document.new(modified_text)
    #assert_select doc.root, 'a[href=?]', url_text
  end
end