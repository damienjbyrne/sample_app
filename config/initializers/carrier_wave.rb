if Rails.env.production?
#   CarrierWave.configure do |config|
#     config.fog_credentials = {
#       # config for dropbox API
#       :provider                    => 'dropbox',
#       :dropbox_oauth2_access_token => ENV['DROPBOX_OAUTH2_ACCESS_TOKEN']
#     }
#     config.fog_directory = "sample-app-djb"
#   end

  CarrierWave.configure do |config|
    config.dropbox_app_key = ENV["APP_KEY"]
    config.dropbox_app_secret = ENV["APP_SECRET"]
    config.dropbox_access_token = ENV["ACCESS_TOKEN"]
    config.dropbox_access_token_secret = ENV["ACCESS_TOKEN_SECRET"]
    config.dropbox_user_id = ENV["USER_ID"]
    config.dropbox_access_type = "app_folder"
  end

end