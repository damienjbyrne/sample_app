class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@mondegreen.com"
  layout 'mailer'
end
