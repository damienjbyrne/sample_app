module MicropostsHelper

  def parse_content(content)
    content.gsub(/(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/, '<a href="\0" target="_blank">\0</a>')
  end
end
